﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Destructible
{
    public int HP = 2;

    private Vector3 moveVector;
    // Start is called before the first frame update
    void Start()
    {
        hp = HP;
        collisionDamage = 1;
        moveVector = new Vector3(transform.position.x, -6f, 0f);
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.MoveTowards(transform.position, moveVector, 0.1f);
        DestroyIfOutOfBounds();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Debug.Log(gameObject + " collided with " + otherObject);

        if (((1 << otherObject.layer) & targetLayerMask) != 0)
        {
            Debug.Log("Hit: " + otherObject.name);
            otherObject.GetComponent<Destructible>().Hit(collisionDamage);

            Die("Collided");
        }
    }
}
