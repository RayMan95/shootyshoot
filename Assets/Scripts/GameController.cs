﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public GameObject spawner;

    public TextMeshProUGUI textTime;

    private int lastHeartIndex = 3;
    public GameObject[] hearts;

    private bool sceneRunning;
    private float playTime = 0f;
    private string sceneName = "";

    public float bossFightTime = 5f;
    private bool bossFight = false;
    public GameObject textLevelWon;
    private bool levelWon = false;

    public GameObject textPlayerDied;
    private bool playerDead = false;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }


    // Update is called once per frame
    void Update()
    {

        if (levelWon)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ResetLevel();
                SceneManager.LoadScene(0);
            }
            else return;
        }
        else if (playerDead)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                ResetLevel();
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // Reload scene
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                ResetLevel();
                SceneManager.LoadScene(0); // Load menu
            }
            else return;
        }

        if (sceneRunning)
        {
            playTime += Time.deltaTime;
            textTime.text = (Mathf.Round(playTime * 100) / 100).ToString();
        }

        if (playTime >= bossFightTime && !bossFight)
        {
            StartBossFight();
            bossFight = true;
        }


    }

    private void ResetLevel()
    {
        bossFight = false;
        levelWon = false;
        playerDead = false;
        sceneName = "";
        lastHeartIndex = 3;
    }

    public void StartScene(string name)
    {
        sceneRunning = true;
        sceneName = name;
    }

    private void StartBossFight()
    {
        spawner.GetComponent<Spawner>().PrepareBossFight();
    }

    internal void BossKilled()
    {
        // TODO
        Debug.Log("Boss Killed");
        sceneRunning = false;
        textLevelWon.SetActive(true);
        levelWon = true;
    }

    internal void PlayerDied()
    {
        // TODO
        Debug.Log("Player Died");
        sceneRunning = false;
        textPlayerDied.SetActive(true);
        playerDead = true;
    }

    internal void LostHealth()
    {
        hearts[lastHeartIndex].SetActive(false);

        lastHeartIndex -= 1;
    }

}

