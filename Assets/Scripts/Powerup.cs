﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    public enum PowerupType
    {
        shield,
        fireRate,
        numberOfGuns
    }

    //public IncomingObject incomingObject;
    public float moveUnit = 0.1f;
    public PowerupType type;

    private Vector3 moveVector;
    // Start is called before the first frame update
    void Start()
    {
    }

    private void FixedUpdate()
    {
        gameObject.GetComponent<Rigidbody2D>().MovePosition(new Vector2(transform.position.x, transform.position.y - moveUnit));

        if (transform.position.y < -6) Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObject = collision.gameObject;

        otherObject.GetComponent<Player>().Pickup(type);
        Debug.Log(type.ToString() + " picked up");

        Destroy(gameObject);
    }
}
