﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
    public float speed = 40f;
    public int damage = 1;

    [SerializeField]
    public LayerMask targetLayerMask;

    private readonly float moveAmount = 0.1f;

    private Rigidbody2D rb2d;

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > 5.5 || transform.position.y < -5.5)
        {
            Destroy(gameObject);
        }
    }

    // TODO: make laser shoot at player's snapshot position
    private void FixedUpdate()
    {
        Vector3 move = new Vector3(0f, moveAmount, 0f);
        rb2d.MovePosition(transform.position + (move * speed * Time.deltaTime));
    }

    // TODO
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObject = collision.gameObject;

        if (((1 << otherObject.layer) & targetLayerMask) != 0)
        {
            Debug.Log("Hit: " + otherObject.name);
            otherObject.GetComponent<Destructible>().Hit(damage);
            Destroy(gameObject);
        }
    }
}
