﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class Player : Destructible
{
    #region Components
    public GameObject shield;
    private Rigidbody2D rb2D;
    #endregion

    #region PublicStats
    public int HP = 4;
    public float speed = 10f;
    public float fireRate = 0.2f;
    public float shieldTime = 4f;
    public float additionalGunsTime = 6f;
    #endregion

    #region OtherPublic
    public GameObject laserPrefab;
    public GameObject[] gunPositions;
    #endregion

    private Vector2 mov = new Vector2(0f, 0f);
    private bool shieldActive;

    private float lastShot;
    private float shieldActiveTime;

    private float additionalGunsActiveTime;
    private int numberOfGuns = 1;
    private int[] gunOrientation = { 1, 0, 0};

    private void Awake()
    {
        hp = HP;
        rb2D = GetComponent<Rigidbody2D>();
        shieldActive = false;
        lastShot = fireRate;
    }

    //private void Start()
    //{
    //}

    // Update is called once per frame
    void Update()
    {
        mov.x = Input.GetAxis("Horizontal");
        mov.y = Input.GetAxis("Vertical");
        

        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
        {
            lastShot -= Time.deltaTime;
            if (lastShot <= 0)
            {
                Shoot();
                lastShot = fireRate;
            }
            
        }
        if (shieldActive)
        {
            shieldActiveTime -= Time.deltaTime;
            if(shieldActiveTime <= 0)
            {
                ActivateShield(false);
            }
        }
        if (numberOfGuns > 1)
        {
            additionalGunsActiveTime -= Time.deltaTime;
            if (additionalGunsActiveTime <= 0)
            {
                numberOfGuns = 1;
                ChangeGunOrientation();
            }
        }
    }

    private void FixedUpdate()
    {
        if(mov.magnitude > 0) Move();
    }

    private void Move()
    {
        Debug.Log(mov);
        rb2D.MovePosition((Vector2)transform.position + (mov * speed * Time.deltaTime));
    }

    // Can later expand to include types of shoot
    private void Shoot()
    {
        for (int i = 0; i < 3; i++)
        {
            if (gunOrientation[i] > 0)
            {
                Instantiate(laserPrefab, gunPositions[i].transform.position, Quaternion.identity);
            }
        }
    }

    override internal void Hit(int damage)
    {
        if (shieldActive) return;
        else
        {
            base.Hit(damage);
            GameController.instance.LostHealth();
        }
    }

    internal void Pickup(Powerup.PowerupType type)
    {
        if (type == Powerup.PowerupType.shield)
        {
            ActivateShield(true);
        }
        else if (type == Powerup.PowerupType.numberOfGuns)
        {
            additionalGunsActiveTime = additionalGunsTime;
            numberOfGuns += 1;
            ChangeGunOrientation();
        }
    }

    private void ActivateShield(bool state)
    {
        shield.SetActive(state);
        shieldActive = state;
        shieldActiveTime = shieldTime;
    }
    private void ChangeGunOrientation()
    {
        if (numberOfGuns == 1)
        {
            gunOrientation[0] = 1;
            gunOrientation[1] = 0;
            gunOrientation[2] = 0;
        }
        else if (numberOfGuns == 2)
        {
            gunOrientation[0] = 0;
            gunOrientation[1] = 1;
            gunOrientation[2] = 1;
        }
        else if (numberOfGuns == 3)
        {
            gunOrientation[0] = 1;
            gunOrientation[1] = 1;
            gunOrientation[2] = 1;
        }
    }

    override internal void Die(string reason)
    {
        GameController.instance.PlayerDied();
        base.Die("Died");
    }

}
