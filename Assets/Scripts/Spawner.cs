﻿using System.Collections;
using System.Threading;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] enemyPrefabs; // UFO
    public GameObject powerupPrefab;

    public IncomingObject[] powerupIncomingObjects;

    private readonly Vector3[] powerupSpawnPositions = { new Vector3(-7.3f, 15f, 0), new Vector3(-5.3f, 15f, 0), new Vector3(-1.3f, 15f, 0), new Vector3(1.3f, 15f, 0), new Vector3(5.3f, 15f, 0), new Vector3(7.3f, 15f, 0)};
    public Sprite[] powerupSprites;
    public float timeBetweenPowerups = 3f;
    private float timeSinceLastPowerup;
    //private string[] powerups; // [shield, firerate, guns]
    private int lastPowerup; // todo: track last power up to flip

    //private readonly Vector3 enemySpawnPos = new Vector3(0f, 20f, 0f);
    private readonly Vector3[] enemySpawnPositions = { new Vector3(-7.3f, 19f, 0), new Vector3(-5.3f, 19f, 0), new Vector3(-1.3f, 19f, 0), new Vector3(1.3f, 19f, 0), new Vector3(5.3f, 19f, 0), new Vector3(7.3f, 19f, 0) };
    private readonly Vector3[] landingPositions = { new Vector3(-7.3f, 4f, 0), new Vector3(-5.3f, 4f, 0), new Vector3(-1.3f, 4f, 0), new Vector3(1.3f, 4f, 0), new Vector3(5.3f, 4f, 0), new Vector3(7.3f, 4f, 0) };
    private bool[] landingPositionsFilled = { false, false, false, false, false, false }; // left to right
    private GameObject[] activeEnemies = new GameObject[6];
    public IncomingObject[] enemyIncomingObjects;
    private float timeSinceLastEnemy = 0f;
    public float timeBetweenEnemies = 4f;

    private bool bossFight = false;
    private bool bossSpawned = false;
    public GameObject bossPrefab;
    private GameObject boss;

    private void Awake()
    {
        timeSinceLastPowerup = timeBetweenPowerups;
        timeSinceLastEnemy = timeBetweenEnemies;
    }

    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        // Always spawn powerups
        timeSinceLastPowerup -= Time.deltaTime;
        if (timeSinceLastPowerup <= 0)
        {
            //if (lastPowerup.Equals(nextPowerup))
            StartCoroutine("SpawnPowerup");
            timeSinceLastPowerup = timeBetweenPowerups;
        }

        if (!bossFight) // Only spawn enemies when not fighting the boss
        {
            timeSinceLastEnemy -= Time.deltaTime;
            if (timeSinceLastEnemy <= 0)
            {
                //if (lastPowerup.Equals(nextPowerup))
                StartCoroutine("SpawnEnemy");
                timeSinceLastEnemy = timeBetweenEnemies;
            }

            UpdateLiveEnemies();
        }

        else
        {
            if (EnemiesAlive())
            {
                UpdateLiveEnemies();
            }
            else if (!EnemiesAlive() && !bossSpawned)
            {
                boss = Instantiate(bossPrefab, new Vector3(0f, 15f, 0f), Quaternion.identity);
                bossSpawned = true;
            }
            else if (bossSpawned && boss == null) GameController.instance.BossKilled();
        }
    }

    public void PrepareBossFight()
    {
        
        bossFight = true;
    }

    IEnumerable SpawnPowerup()
    {
        int nextPowerup = Random.Range(0, powerupSprites.Length); 
        
        GameObject spawn = Instantiate(powerupPrefab, powerupSpawnPositions[Random.Range(0, powerupSpawnPositions.Length)], Quaternion.identity);

        Powerup p = spawn.GetComponent<Powerup>();
        p.type = (Powerup.PowerupType)nextPowerup;
        
        spawn.GetComponent<SpriteRenderer>().sprite = powerupSprites[nextPowerup];

        return null;
    }

    IEnumerable SpawnEnemy()
    {

        int type = Random.Range(0, enemyPrefabs.Length);

        int index = Random.Range(0, enemyIncomingObjects.Length);
        if (landingPositionsFilled[index] && type == 1)
        {
            for (index = 0; index <= landingPositions.Length; index++)
            {
                if (index == landingPositionsFilled.Length) 
                    return null;

                if (!landingPositionsFilled[index]) break;
            }
        }        


        GameObject spawn = Instantiate(enemyPrefabs[type], enemySpawnPositions[index], Quaternion.identity);

        if (type == 1)
        {
            landingPositionsFilled[index] = true; // hardcoded
            activeEnemies[index] = spawn;
        }

        Enemy e = spawn.GetComponent<Enemy>();

        e.incomingObject = enemyIncomingObjects[index];

        return null;
    }

    // update list if enemies have died
    private void UpdateLiveEnemies()
    {
        for (int i = 0; i < 6; i++)
        {
            if (activeEnemies[i] == null) landingPositionsFilled[i] = false;
        }
    }

    private bool EnemiesAlive()
    {
        return (landingPositionsFilled[0] || landingPositionsFilled[1] || landingPositionsFilled[2] || landingPositionsFilled[3] || landingPositionsFilled[4] || landingPositionsFilled[5]);
    }
}
