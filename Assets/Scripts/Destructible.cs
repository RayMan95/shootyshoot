﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Destructible : MonoBehaviour
{
    
    internal int hp;
    internal int collisionDamage;

    public LayerMask targetLayerMask;

     virtual internal void Hit(int damage)
     {
        hp -= damage;
        if (hp <= 0) Die("Died");
     }

    internal virtual void Die(string destructionReason)
    {
        Debug.Log(destructionReason + ": " + gameObject.name);
        Destroy(gameObject);
    }

    

    internal void DestroyIfOutOfBounds()
    {
        if (transform.position.y <= -10)
        {
            Die("OutofBounds");
        }
    }
}
