﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewIncomingObject", menuName = "IncomingObject")]
public class IncomingObject : ScriptableObject
{
    public bool isStatic;
    
    public float speed;
    
    public Vector3 landingLocation;
    public Vector3 endLocation;
}
