﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour
{
    bool waitingToStart = true;
    bool waitingForMove = false;
    bool pressedW = false;
    bool pressedA = false;
    bool pressedS = false;
    bool pressedD = false;
    bool waitingForShot = false;
    bool waitingForTargetsDestroyed = false;
    bool waitingToQuit = false;

    private float waitTime = 3f;
    private float shootTime = 0.5f;

    public GameObject player;
    public GameObject[] texts;

    public GameObject[] targets;

    // Update is called once per frame
    void Update()
    {
        if (waitingToStart)
        {
            waitTime -= Time.deltaTime;

            if (waitTime <= 0)
            {
                StartTutorial();
            }
        }
        else if(waitingForMove)
        {
            if(Input.GetKey(KeyCode.W))
            {
                texts[2].SetActive(false);
                pressedW = true;
            }
            if (Input.GetKey(KeyCode.A))
            {
                texts[3].SetActive(false);
                pressedA = true;
            }
            if (Input.GetKey(KeyCode.S))
            {
                texts[4].SetActive(false);
                pressedS = true;
            }
            if (Input.GetKey(KeyCode.D))
            {
                texts[5].SetActive(false);
                pressedD = true;
            }

            if (pressedW && pressedA && pressedS && pressedD)
            {
                StartShoot();
            }
        }
        else if (waitingForShot)
        {
            if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
            {
                shootTime -= Time.deltaTime;
                if(shootTime <= 0)
                {
                    StartDestroy();
                }
            }
        }
        else if (waitingForTargetsDestroyed)
        {
            foreach (GameObject target in targets)
            {
                if (target)
                {
                    return;
                }
            }

            StartEndTutorial();
        }
        else if (waitingToQuit)
        {
            if(Input.GetKey(KeyCode.Escape))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    private void StartTutorial()
    {
        waitingToStart = false;
        texts[0].SetActive(false);
        
        player.SetActive(true);
        texts[1].SetActive(true);
        texts[2].SetActive(true);
        texts[3].SetActive(true);
        texts[4].SetActive(true);
        texts[5].SetActive(true);
        
        waitingForMove = true;
    }

    private void StartShoot()
    {
        waitingForMove = false;

        texts[1].SetActive(false);
        texts[2].SetActive(false);
        texts[3].SetActive(false);
        texts[4].SetActive(false);
        texts[5].SetActive(false);

        texts[6].SetActive(true);
        texts[7].SetActive(true);
        waitingForShot = true;
    }
    private void StartDestroy()
    {
        waitingForShot = false;
        texts[7].SetActive(false);

        foreach (GameObject target in targets)
        {
            target.SetActive(true);
        }
        waitingForTargetsDestroyed = true;
    }
    private void StartEndTutorial()
    {
        waitingForTargetsDestroyed = false;
        texts[6].SetActive(false);

        texts[8].SetActive(true);
        texts[9].SetActive(true);
        player.SetActive(false);

        waitingToQuit = true;
    }
}