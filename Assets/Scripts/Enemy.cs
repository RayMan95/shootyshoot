﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Destructible
{
    #region Components
    private Rigidbody2D rb2D;
    #endregion

    #region OtherPublic
    public GameObject laserPrefab;
    public IncomingObject incomingObject;
    #endregion

    #region PublicStats
    public int HP = 4;
    public float fireRate = 1f;
    public bool docile = false;
    public float waitTime = 3f;
    #endregion

    private float lastFired = 0f;
    private bool active;
    private bool shooting = false;

    private bool waiting = false;
    private float waitingTime;


    // Start is called before the first frame update
    void Awake()
    {
        active = false;
        hp = HP;
        lastFired = fireRate;
        collisionDamage = 2;
        waitingTime = waitTime; 
    }

    // Update is called once per frame
    void Update()
    {
        if (waiting)
        {
            waitingTime -= Time.deltaTime;
            if (waitingTime <= 0)
            {
                active = true;
                
                waiting = false;

                if (!docile) shooting = true; ;
            }
            return;
        }

        if (active)
        {
            if (!docile)
            {
                if (shooting)
                {
                    if (lastFired <= 0)
                    {
                        Shoot();
                        lastFired = fireRate;
                    }
                    else
                    {
                        lastFired -= Time.deltaTime;
                    }
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (!shooting) Move();
    }

    // Need to simlify this system by making: docile always go down or diagonal and non-docile always stop and shoot
    void Move()
    {
        if (!active)
        {
            transform.position = Vector2.MoveTowards(transform.position, incomingObject.landingLocation, 0.1f);
            if (transform.position == incomingObject.landingLocation)
            {
                waiting = true;
            }
                
        }
            
        else transform.position = Vector2.MoveTowards(transform.position, incomingObject.endLocation, 0.2f);
        DestroyIfOutOfBounds();
    }

    private void Shoot()
    {
        Vector3 redLaserOffset = new Vector3(0f, -0.5f, 0f);
        Instantiate(laserPrefab, transform.position + redLaserOffset, Quaternion.identity);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject otherObject = collision.gameObject;
        Debug.Log(gameObject + " collided with " + otherObject);

        if (((1 << otherObject.layer) & targetLayerMask) != 0)
        {
            Debug.Log("Hit: " + otherObject.name);
            otherObject.GetComponent<Destructible>().Hit(collisionDamage);

            Die("Collided");
        }
    }

    //internal new void Die(string reason)
    //{
        
    //    base.Die(reason);
    //}
}
