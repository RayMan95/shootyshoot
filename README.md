Test project made to learn making a third-person shooter in Unity.

Core things I want to achieve with this:
1. Playable shooter character with powerups.
2. At least 2 types of base enemies and a boss per level.
3. At least 2 levels.
4. A tutorial.

Third-party Assets:
* Space Shooter Pack from Kenney - https://kenney.itch.io/kenney-game-assets-1